export const API_URI = 'https://api.graph.cool/simple/v1/ciyp5mu7q47df0132db4vojmp';

export const LOCAL_STORAGE_KEY = 'paep';

export const LOCAL_STORAGE_AUTH_KEY = `${LOCAL_STORAGE_KEY}:auth0IdToken`;
