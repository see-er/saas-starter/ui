export const blue10 = '#bfe6ff';
export const blue20 = '#79c6ff';
export const blue30 = '#56a8fd';
export const blue40 = '#5294e9';
export const blue50 = '#3e76c0';
export const blue60 = '#305b81';
export const blue70 = '#244a61';
export const blue80 = '#1c364a';
export const blue90 = '#142936';
export const blue100 = '#010205';

export const green10 = '#c7f18a';
export const green20 = '#b3e244';
export const green30 = '#8ad400';
export const green40 = '#57a900';
export const green50 = '#498500';
export const green60 = '#2b6700';
export const green70 = '#114e10';
export const green80 = '#063d00';
export const green90 = '#0a2906';
export const green100 = '#010200';

export const teal10 = '#a4fbe6';
export const teal20 = '#68eed8';
export const teal30 = '#36d7c3';
export const teal40 = '#00b5a0';
export const teal50 = '#008671';
export const teal60 = '#006d5d';
export const teal70 = '#005448';
export const teal80 = '#003c32';
export const teal90 = '#002b22';
export const teal100 = '#000202';

export const purple10 = '#efd1ff';
export const purple20 = '#d8a8ff';
export const purple30 = '#bb8cfa';
export const purple40 = '#b06aeb';
export const purple50 = '#9950d7';
export const purple60 = '#743d9a';
export const purple70 = '#572c74';
export const purple80 = '#422157';
export const purple90 = '#311842';
export const purple100 = '#030103';

export const red10 = '#ffd2dd';
export const red20 = '#ffa4b3';
export const red30 = '#ff7c85';
export const red40 = '#ff4e4b';
export const red50 = '#ea172a';
export const red60 = '#af1220';
export const red70 = '#8e0d18';
export const red80 = '#6f071c';
export const red90 = '#4d0816';
export const red100 = '#040001';

export const grey10 = '#e0e0e0';
export const grey20 = '#c7c7c7';
export const grey30 = '#aeaeae';
export const grey40 = '#959595';
export const grey50 = '#777677';
export const grey60 = '#5a5a5a';
export const grey70 = '#464646';
export const grey80 = '#323232';
export const grey90 = '#121212';
export const grey100 = '#000000';

export const black = '#000000';

export const white = '#ffffff';
export const white1 = '#fdfdfd';
export const white2 = '#f9f9f9';
export const white3 = '#f4f4f4';
export const white4 = '#ececec';
